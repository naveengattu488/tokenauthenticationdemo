﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TokenBasedAuth.Migrations
{
    public partial class RegisterUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    EmployeeId = table.Column<string>(nullable: false),
                    ProjectName = table.Column<string>(nullable: false),
                    Email = table.Column<string>(nullable: false),
                    Mobile = table.Column<string>(nullable: false),
                    FirstName = table.Column<string>(nullable: false),
                    LastName = table.Column<string>(nullable: false),
                    Username = table.Column<string>(nullable: false),
                    Password = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.EmployeeId);
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "EmployeeId", "Email", "FirstName", "LastName", "Mobile", "Password", "ProjectName", "Username" },
                values: new object[] { "2352633", "naveenkumar.gattu@mphasis.com", "naveen", "gattu", "9550866617", "naveen123", "SAC", "naveen" });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "EmployeeId", "Email", "FirstName", "LastName", "Mobile", "Password", "ProjectName", "Username" },
                values: new object[] { "2352611", "suryakanth.sudharshanam@mphasis.com", "suryakanth", "sudharshanam", "703158956", "surya123", "DAO", "surya" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Users");
        }
    }
}
