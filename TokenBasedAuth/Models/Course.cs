﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TokenBasedAuth.Models
{
    public class Course
    {
        public string SourceName { get; set; }
        public int HoursSpent { get; set; }
        public string StartDate { get; set; }
        public string Status { get; set; }
        public string EndDate { get; set; }
    }

    public class SourceDetails
    {
        public string sourceName { get; set; }
        public double Percentage { get; set; }
    }

    public class CoursesByDay
    {
        public string Day { get; set; }
        public int CoursesCompleted { get; set; }
        public int CoursesBegan { get; set; }
        public int noOfHoursSpent { get; set; }
    }


}
