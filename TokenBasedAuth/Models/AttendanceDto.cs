﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TokenBasedAuth.Models
{
    public class AttendanceDto
    {
        public string AprilDate { get; set; }
        public int WFCPresent { get; set; }
        public int WFOPresent { get; set; }
    }
}
