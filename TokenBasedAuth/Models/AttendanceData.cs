﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TokenBasedAuth.Models
{
    public class AttendanceData
    {
        public string Name { get; set; }
        public string WorkStatus { get; set; }
        public IDictionary<string, string> PresentStatus { get; set; } = new Dictionary<string, string>();
    }

    public class PresentData 
    {
        public string EmployeeName { get; set; }
        public int NoOfDaysPresent { get; set; }
        public int NoOfDaysAbsent { get; set; }
        public int TotalNoOfDays { get; set; }

    }

}
