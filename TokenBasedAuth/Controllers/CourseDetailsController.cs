﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ExcelDataReader;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TokenBasedAuth.Models;

namespace TokenBasedAuth.Controllers
{
    [Authorize]
    [EnableCors("Policy1")]
    [Route("api/[controller]")]
    [ApiController]
    public class CourseDetailsController : ControllerBase
    {
        public CourseDetailsController()
        {
                
        }
        [HttpGet]
        public IActionResult GetCourseDetails()
        {
            List<Course> courses = new List<Course>();
            var fileName = "./CourseDetails.xlsx";
            // For .net core, the next line requires NuGet package, 
            // System.Text.Encoding.CodePages
            System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
            using (var stream = System.IO.File.Open(fileName, FileMode.Open, FileAccess.Read))
            {
                using (var reader = ExcelReaderFactory.CreateReader(stream))
                {
                    //Removing the 1st row from excel sheet
                    var readFirstRecord = reader.Read();
                    while (reader.Read()) //Each ROW
                    {
                        //var course
                        courses.Add(new Course()
                        {
                            StartDate = reader.GetValue(6).ToString(),
                            HoursSpent = Convert.ToInt32(reader.GetValue(8)),
                            EndDate= reader.GetValue(7).ToString(),
                            Status = reader.GetValue(9).ToString()
                        });
                    }
                }
            }

            var DistincStarttDates = courses.Select(x => x.StartDate).Distinct().ToList();
            var DistinctEndDates = courses.Select(x => x.EndDate).Distinct().ToList();
            var distinctDates = DistincStarttDates.Concat(DistinctEndDates).ToList().Distinct();

            IList<CoursesByDay> coursesByDays = new List<CoursesByDay>();

            foreach(var date in distinctDates)
            {
                coursesByDays.Add(
                    new CoursesByDay()
                    {
                        Day = date.Substring(0,6),
                        CoursesCompleted = courses.FindAll(x => x.EndDate == date).Where(x=> x.Status == "Completed").Count(),
                        noOfHoursSpent = courses.FindAll(x => x.EndDate == date).Select(x => x.HoursSpent).Sum(),
                        CoursesBegan = courses.FindAll(x => x.StartDate == date).Where(x => x.StartDate != x.EndDate).Count()
                    });
            }

            return Ok(coursesByDays);
        }
    }
}