﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ExcelDataReader;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TokenBasedAuth.Models;

namespace TokenBasedAuth.Controllers
{
    [Route("api/[controller]")]
    [EnableCors("Policy1")]
    [Authorize]
    [ApiController]
    public class AttendanceController : ControllerBase
    {
        public AttendanceController()
        {

        }
        [HttpGet]
        public IActionResult GetAttendanceData([FromQuery] string? worktype)
        {
            {
                List<AttendanceData> users = new List<AttendanceData>();
                var fileName = "./Users.xlsx";
                // For .net core, the next line requires NuGet package, 
                // System.Text.Encoding.CodePages
                System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
                using (var stream = System.IO.File.Open(fileName, FileMode.Open, FileAccess.Read))
                {
                    using (var reader = ExcelReaderFactory.CreateReader(stream))
                    {

                        while (reader.Read()) //Each ROW
                        {
                            int i = 0;
                            var attendanceData = new AttendanceData();

                            //store each employee by date and present status

                            attendanceData.Name = reader.GetValue(i).ToString();
                            attendanceData.WorkStatus = reader.GetValue(i + 1).ToString();

                            for (int j = 1, k = 2; j <= reader.FieldCount - 2; j++, k++)
                            {
                                //var dictionaryObj = new Dictionary<string, string>() { { (j + 1).ToString(), reader.GetValue(j + 2).ToString() } };
                                attendanceData.PresentStatus.Add(new KeyValuePair<string, string>(j.ToString(), reader.GetValue(k).ToString()));
                                //Thread.Sleep(100);
                            }

                            users.Add(attendanceData);
                        }
                    }
                }
                users.RemoveAt(0);

                List<AttendanceDto> attendanceByDate = new List<AttendanceDto>();

                for(int item=1; item < users.Select(x => x.PresentStatus).FirstOrDefault().Count(); item++)
                {
                    var attendanceObj = new AttendanceDto() 
                    { 
                        AprilDate = item.ToString(), 
                        WFCPresent= users.Where(item => item.WorkStatus == "WFH").Select(i => i.PresentStatus[item.ToString()]).ToList().Where(i => i.Contains("P")).Count(),
                        WFOPresent = users.Where(item => item.WorkStatus == "WFO").Select(i => i.PresentStatus[item.ToString()]).ToList().Where(i => i.Contains("P")).Count()
                    };
                    attendanceByDate.Add(attendanceObj);
                }

                if (string.IsNullOrWhiteSpace(Request.Query["worktype"].ToString()))
                {
                    return Ok(attendanceByDate);
                }

                if (worktype != null)
                {
                    IList<PresentData> presentData = new List<PresentData>();
                    var wfhNames = users.FindAll(x => x.WorkStatus == worktype).Select(x => x.Name).Distinct().ToList();
                    int totalDays = users.Select(x => x.PresentStatus).FirstOrDefault().Count;

                  foreach(var name in wfhNames)
                  {
                        var empData = users.Where(x => x.Name == name).FirstOrDefault();
                        int presentCount = 0;
                        int absentCount = 0;
                        int notAvailable = 0;
                        foreach (KeyValuePair<string, string> entry in empData.PresentStatus)
                        {
                            if (entry.Value.ToUpper() == "P") { presentCount++; }
                            if (entry.Value.ToUpper() == "A") { absentCount++; }
                            if (entry.Value.ToUpper()== "NA") { notAvailable++; }
                        }
                        presentData.Add(new PresentData() {EmployeeName=name, NoOfDaysPresent=presentCount, NoOfDaysAbsent=absentCount, TotalNoOfDays= totalDays-notAvailable });
                  }
                    return Ok(presentData);
                }
                return BadRequest();
            }
        }
    }
}