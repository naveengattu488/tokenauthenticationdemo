﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TokenBasedAuth.Models;
using TokenBasedAuth.Services;

namespace TokenBasedAuth.Controllers
{
    [ApiController]
    [EnableCors("Policy1")]
    [Route("api/[controller]")]
    public class RegistrationController: ControllerBase
    {
        private readonly ILogger<RegistrationController> _logger;
        private readonly IUserService _UserService;

        public RegistrationController(ILogger<RegistrationController> logger, IUserService userService)
        {
            _logger = logger;
            _UserService = userService;
        }

        [HttpPost]
        public IActionResult RegisterUser(UserDetailsDto user)
        {
            if (_UserService.UserExists(user.EmployeeId))
            {
                ModelState.AddModelError("EmployeeId","The User with given employee Id exists");
                return BadRequest(ModelState);
            }
            _UserService.RegisterUser(user);

            _UserService.SaveData();

            return NoContent();
        }
    }
}
