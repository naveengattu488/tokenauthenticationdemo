﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ExcelDataReader;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TokenBasedAuth.Models;

namespace TokenBasedAuth.Controllers
{
    [Authorize]
    [EnableCors("Policy1")]
    [Route("api/[controller]")]
    [ApiController]
    public class CourseController : ControllerBase
    {
        [HttpGet]
        public IActionResult GetCourse()
        {
            List<Course> courses = new List<Course>();
            var fileName = "./CourseDetails.xlsx";
            // For .net core, the next line requires NuGet package, 
            // System.Text.Encoding.CodePages
            System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
            using (var stream = System.IO.File.Open(fileName, FileMode.Open, FileAccess.Read))
            {
                using (var reader = ExcelReaderFactory.CreateReader(stream))
                {
                    //Removing the 1st row from excel sheet
                    var readFirstRecord = reader.Read();
                    while (reader.Read()) //Each ROW
                    {
                        //var course
                        courses.Add(new Course()
                        {
                            SourceName = reader.GetValue(5).ToString(),
                            //StartDate = reader.GetValue(6).ToString(),
                            //HoursSpent = Convert.ToInt32(reader.GetValue(8))
                        });
                    }
                }
            }

            var distinctCourses = courses.Select(x => x.SourceName.ToUpper().Trim()).Distinct();
            int totalCourses = courses.Select(x => x.SourceName).Count();

            List<SourceDetails> sourceInformation = new List<SourceDetails>();

            foreach(var course in distinctCourses)
            {
                sourceInformation.Add(new SourceDetails()
                {
                    sourceName = course,
                    Percentage = Math.Round((double)(courses.FindAll(x => x.SourceName.ToUpper().Contains(course)).Count()*100)/ totalCourses,2)
                });
            }

            return Ok(sourceInformation);
        }
    }
}

