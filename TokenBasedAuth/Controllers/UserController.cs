﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using TokenBasedAuth.Data;
using TokenBasedAuth.Models;
using TokenBasedAuth.Services;

namespace TokenBasedAuth.Controllers
{
    [Authorize]
    [ApiController]
    [EnableCors("Policy1")]
    [Route("api/[controller]")]
    public class UserController : ControllerBase
    {
        private readonly ILogger<UserController> _logger;
        private readonly IUserService _UserService;

        public UserController(ILogger<UserController> logger, IUserService userService)
        {
            _logger = logger;
            _UserService = userService;
        }

        [HttpGet]
        public ActionResult<UserDetailsDto> Get()
        {
           var users=_UserService.GetUsers();

            if(users == null)
            {
                return NotFound();
            }

           var userDetails = new List<UserDetailsDto>();

            foreach(var user in users)
            {
                userDetails.Add(new UserDetailsDto() { 
                EmployeeId = user.EmployeeId,
                ProjectName = user.ProjectName.ToUpper(),
                Email = user.Email,
                Mobile = user.Mobile,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Username = user.Username,
                Password = user.Password
            });
            }

            return Ok(userDetails);
        }
    }
}
