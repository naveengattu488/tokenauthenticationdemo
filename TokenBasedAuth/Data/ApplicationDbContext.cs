﻿using Microsoft.EntityFrameworkCore;

namespace TokenBasedAuth.Data
{
    public interface IApplicationDbContext
    {
        DbSet<User> Users { get; set; }
    }
    public class ApplicationDbContext : DbContext, IApplicationDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }
       public DbSet<User> Users { get; set; }
       
       protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().HasData(
                new User() { EmployeeId="2352633", ProjectName="SANKALP", Email="naveenkumar.gattu@mphasis.com", Mobile="9550866617", FirstName="naveen", LastName="gattu", Username="naveen", Password="naveen123"},
                new User() { EmployeeId = "2352611", ProjectName = "DAO", Email = "suryakanth.sudharshanam@mphasis.com", Mobile = "703158956",FirstName = "suryakanth", LastName = "sudharshanam", Username = "surya", Password = "surya123" }
                );
        }
    }
}
