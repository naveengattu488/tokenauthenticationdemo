﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TokenBasedAuth.Data;
using TokenBasedAuth.Models;

namespace TokenBasedAuth.Services
{
    public interface IUserService
    {
        bool IsValidUser(UserModel user);
        public IEnumerable<User> GetUsers();
        public void RegisterUser(UserDetailsDto user);
        public bool SaveData();
        public bool UserExists(string employeeId);
    }
}
