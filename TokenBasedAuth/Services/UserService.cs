﻿using System.Collections.Generic;
using System.Linq;
using TokenBasedAuth.Data;
using TokenBasedAuth.Models;

namespace TokenBasedAuth.Services
{
    public class UserService : IUserService
    {
        private readonly ApplicationDbContext _context;
        public UserService(ApplicationDbContext context)
        {
            _context = context;
        }

        public IEnumerable<User> GetUsers()
        {
            return _context.Users.ToList();
        }

        public bool IsValidUser(UserModel user)
        {
            return _context.Users.Any(x => x.Username.ToLower().Equals(user.Username.ToLower()) && x.Password.Equals(user.Password));
        }

        public void RegisterUser(UserDetailsDto user)
        {
            if (user != null)
            {
                var userEntity = new User();
                userEntity.EmployeeId = user.EmployeeId;
                userEntity.ProjectName = user.ProjectName.ToUpper();
                userEntity.Email = user.Email;
                userEntity.Mobile = user.Mobile;
                userEntity.FirstName = user.FirstName;
                userEntity.LastName = user.LastName;
                userEntity.Username = user.Username;
                userEntity.Password = user.Password;

                _context.Users.Add(userEntity);
            }

        }

        public bool UserExists(string employeeId)
        {
            return _context.Users.Any(x => x.EmployeeId == employeeId);
        }

        public bool SaveData()
        {
            return (_context.SaveChanges() >= 0);
        }
    }
}
